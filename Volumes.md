Containers are typically created to have no persistent data, so when the container is deleted and recreated, no data is lost (Stateles container).

Other containers (Statefull container) require saving this data, to do this, they must be saved in volumes.

The volumes are stored in the node where the container is deployed, if this container moves from node or is deployed in more than one node at the same time, the data will not be the same, what causes errors, in these cases there are 2 solutions:
* Create shared volumes.
* Set the node where the service will be deployed, so that it always uses the same volume.

For each service you must decide what will be used.

**Volumes require backups, no matter what type of volume is.**

# Shared mounts
Volumes can be shared with 2 technologies.

## NFS
A server shares the data and the rest of the nodes mount it from their O.S., this connection is not encrypted. If the NFS server is stopped, the data is no longer accessible.

All data that is read or written is transferred over the network continuously.

## GlusterFS
All Docker nodes create a GlusterFS network, this network is similar to a RAID5, so all nodes have all or part of the data. Connection between nodes is encrypted.

When writing a new data, GlusterFS synchronizes it with the rest of the nodes, reading is faster since the data is usually locally.

If one of the nodes is stopped, the rest still have the data.

Because the data is replicated, the required size is bigger.

In both cases, the folders will be created in /mnt.
