This repository has the deployment files and configurations according to the [Swarm Project](https://www.moon.cat/docs/swarm/introduction/).

The explanation of this project is only available in Spanish, but each of the yaml files has documentation in English that should be enough to understand how it work.
